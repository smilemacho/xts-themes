# Dark Theme for Xfce4 Themes Switcher

Nice own Dark theme for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

This theme require Xfce4, Xfce4 Theme Switcher, Arc theme, Plank, Conky, Sardi icons and Whisker menu.


![Dark-theme](img-git/Dark-theme.png)

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-dark-theme/):
```
git clone https://aur.archlinux.org/xts-dark-theme.git
cd xts-dark-theme/
makepkg -sci
```

### From the binary package (Debian-based Linux distributions):

Download the latest sardi-icons package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

Download the latest xts-dark-theme package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

After download you need then run this command (like root):

```
apt install ./sardi-icons*.deb
apt install ./xts-dark-theme*.deb
```


### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package

For install Sardi icons from AUR run these commands (like normal user):

```
git clone https://aur.archlinux.org/sardi-icons.git
cd sardi-icons
makepkg -sci
cd ..
rm -rf sardi-icons
```

You need these packages for building the ArchLinux package:

```
base-devel git wget yajl sardi-icons xfce4-theme-switcher
xfce4-whiskermenu-plugin arc-gtk-theme gtk-engines plank conky
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:

Run command (like root):

```
pacman -U distrib/xts-dark-theme*.pkg.tar.xz
```

### Build Debian own package
You need these packages for building the Debian package:

A first you need download and install [sardi-icons](https://github.com/entexsoft/deb-packages/tree/master/packages).

```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
sardi-icons xfce4-theme-switcher xfce4-whiskermenu-plugin arc-theme plank conky

```

Run command (like root):
```
make build-deb
```
### Install from *.deb package:
```
apt install ./distrib/xts-dark-theme*.deb
```
