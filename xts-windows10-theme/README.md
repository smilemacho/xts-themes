# Windows 10 Themes for Xfce4 Themes Switcher

Three nice Windows 10 themes for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

This themes require Xfce4, Xfce4 Theme Switcher and Whisker menu.

## Linux 10

![Linux-10](img-git/Linux-10.png)

## Windows 10 Dark blue

![Windows-10-db](img-git/Windows-10-db.png)

## Windows 10 Light blue

![Windows-10-lb](img-git/Windows-10-lb.png)


## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-windows10-theme/):
```
git clone https://aur.archlinux.org/xts-windows10-theme.git
cd xts-windows10-theme/
makepkg -sci
```

### From the binary package (Debian-based Linux distributions):

Download the latest xts-windows10-theme package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

After download you need then run this command (like root):

```
apt install ./xts-windows10-theme*.deb
```


### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/xts-windows10-theme*.pkg.tar.xz
```

### Build Debian own package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```
### Install from *.deb package:
```
apt install ./distrib/xts-windows10-theme*.deb
```
